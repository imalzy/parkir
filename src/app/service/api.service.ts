import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { from, Observable, throwError } from 'rxjs';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';
import { Z_DATA_ERROR } from 'zlib';

@Injectable({
    providedIn: 'root'
})

export class ApiService {
    BaseURL = environment.BaseUrl;
    constructor(
        private http: HttpClient
    ) { }

    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(
                'Backend returned code ${error.status}, ' +
                'body was: ${error.error}');
        }
        // return an observable with a user-facing error message
        return throwError(
            'Something bad happened; please try again later.');
    };

    login_post(data): Observable<any> {
        const formData: FormData = new FormData();
        let headers = new HttpHeaders({
            'Content-Type': 'application/json'
        });
        let options = {
            headers: headers
        };

        return this.http.post(this.BaseURL + 'auth/login/', data, options)
            .pipe(map(response => {
                return response;
            }));
    }

    chekin_get(): Observable<any> {
        let headers = new HttpHeaders({
            'Content-Type': 'application/json'
        });
        let options = {
            headers: headers
        };

        return this.http.get(this.BaseURL + 'cekin', options)
            .pipe(map(response => {
                return response;
            }));
    }

    chekin_post(data: any): Observable<any> {
        let headers = new HttpHeaders({
            'Content-Type': 'application/json'
        });
        let options = {
            headers: headers
        };

        return this.http.post(this.BaseURL + 'cekin', data, options)
            .pipe(map(response => {
                return response;
            }));
    }

    checkout_post(data: any): Observable<any> {
        let headers = new HttpHeaders({
            'Content-Type': 'application/json'
        });
        let options = {
            headers: headers
        };

        return this.http.post(this.BaseURL + 'cekout', data, options)
            .pipe(map(response => {
                return response;
            }));
    }

    tarif_get(): Observable<any> {
        let headers = new HttpHeaders({
            'Content-Type': 'application/json'
        });
        let options = {
            headers: headers
        };

        return this.http.get(this.BaseURL + 'tarif', options)
            .pipe(map(response => {
                return response;
            }));
    }

    tarif_post(data: any): Observable<any> {
        let headers = new HttpHeaders({
            'Content-Type': 'application/json'
        });
        let options = {
            headers: headers
        };

        return this.http.post(this.BaseURL + 'tarif/tambah/', data, options)
            .pipe(map(response => {
                return response;
            }));
    }

    tarif_put(id: string, data: any): Observable<any> {
        let headers = new HttpHeaders({
            'Content-Type': 'application/json'
        });
        let options = {
            headers: headers
        };

        return this.http.put(this.BaseURL + 'tarif/edit/' + id, data, options)
            .pipe(map(response => {
                return response;
            }));
    }

    user_get(): Observable<any> {
        let headers = new HttpHeaders({
            'Content-Type': 'application/json'
        });
        let options = {
            headers: headers
        };

        return this.http.get(this.BaseURL + 'user', options)
            .pipe(map(response => {
                return response;
            }));
    }


    user_post(data: any): Observable<any> {
        let headers = new HttpHeaders({
            'Content-Type': 'application/json'
        });
        let options = {
            headers: headers
        };

        return this.http.post(this.BaseURL + 'user/tambah/', data, options)
            .pipe(map(response => {
                return response;
            }));
    }

    user_put(id: any, data: any): Observable<any> {
        let headers = new HttpHeaders({
            'Content-Type': 'application/json'
        });
        let options = {
            headers: headers
        };

        return this.http.post(this.BaseURL + 'user/edit/' + id, data, options)
            .pipe(map(response => {
                return response;
            }));
    }
}