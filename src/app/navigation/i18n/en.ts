export const locale = {
    lang: 'en',
    data: {
        'NAV': {
            'APPLICATIONS': 'Applications',
            'SETTINGS': 'Settings',
            // 'SAMPLE'        : {
            //     'TITLE': 'Sample',
            //     'BADGE': '25'
            // },
            'PETUGAS': {
                'TITLE': 'Petugas',
                'Badge': '',
            },
            'MASUK': {
                'TITLE': 'Parkir',
                'Badge': '',
            },
            'KELUAR': {
                'TITLE': 'Keluar',
                'Badge': '',
            },
            'PENGATURAN': {
                'TITLE': 'Pengaturan',
                'Badge': '',
            },
            'TARIF': {
                'TITLE': 'Tarif',
                'Badge': '',
            },
            'USER': {
                'TITLE': 'User',
            },
            'LAPORAN':{
                'TITLE': 'Laporan',
            },
            'EXAMPLE' : {
                'TITLE': 'Example',
            },
            'LOGOUT': {
                'TITLE': 'Logout',
                'BADGE': '25'
            }
        }
    }
};
