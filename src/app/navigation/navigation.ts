import { FuseNavigation } from '@fuse/types';

export const navigation: FuseNavigation[] = [
    {
        id: 'applications',
        title: 'Applications',
        translate: 'NAV.APPLICATIONS',
        type: 'group',
        children: [
            {
                id       : 'masuk',
                title    : 'Parkir',
                translate: 'NAV.MASUK.TITLE',
                type     : 'item',
                icon     : 'local_parking',
                url      : '/checkin',
                // badge    : {
                //     title    : '25',
                //     translate: 'NAV.SAMPLE.BADGE',
                //     bg       : '#F44336',
                //     fg       : '#FFFFFF'
                // }
            },
            // {
            //     id       : 'example',
            //     title    : 'Example',
            //     translate: 'NAV.EXAMPLE.TITLE',
            //     type     : 'item',
            //     icon     : 'description',
            //     url      : '/example',
            // },
            {
                id       : 'laporan',
                title    : 'Laporan',
                translate: 'NAV.LAPORAN.TITLE',
                type     : 'item',
                icon     : 'description',
                url      : '/laporan',
                // badge    : {
                //     title    : '25',
                //     translate: 'NAV.SAMPLE.BADGE',
                //     bg       : '#F44336',
                //     fg       : '#FFFFFF'
                // }
            },
            {
                id: 'pengaturan',
                title: 'Pengaturan',
                translate: 'NAV.PENGATURAN.TITLE',
                type: 'collapsable',
                icon: 'settings',
                children: [
                    {
                        id: 'tarif',
                        title: 'Tarif',
                        translate: 'NAV.TARIF.TITLE',
                        type: 'item',
                        url: '/setting/tarif'
                    },
                    {
                        id: 'user',
                        title: 'User',
                        translate: 'NAV.USER.TITLE',
                        type: 'item',
                        url: '/setting/user',
                    },
                ]
            },
            {
                id: 'Logout',
                title: 'Logout',
                translate: 'NAV.LOGOUT.TITLE',
                type: 'item',
                icon: 'power_settings_new',
                url: '/auth/logout',
                // badge    : {
                //     title    : '25',
                //     translate: 'NAV.CITIES.BADGE',
                //     bg       : '#F44336',
                //     fg       : '#FFFFFF'
                // } settings/user-management
            },
        ]
    }
];
