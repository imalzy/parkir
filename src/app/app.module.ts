import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { TranslateModule } from '@ngx-translate/core';
import 'hammerjs';

import { FuseModule } from '@fuse/fuse.module';
import { FuseSharedModule } from '@fuse/shared.module';
import {
    FuseProgressBarModule, FuseSidebarModule, FuseThemeOptionsModule, FuseCountdownModule,
    FuseHighlightModule, FuseMaterialColorPickerModule, FuseWidgetModule
} from '@fuse/components';

import { fuseConfig } from 'app/fuse-config';
import { ApiService } from './service/api.service';

//app module
import { AppComponent } from 'app/app.component';
import { LayoutModule } from 'app/layout/layout.module';
import { SampleModule } from 'app/main/sample/sample.module';
import { MasukModule } from 'app/main/petugas/masuk/masuk.module';
import { KeluarModule } from 'app/main/petugas/keluar/keluar.module';
import { TarifModule } from 'app/main/setting/tarif/tarif.module';
import { UserModule } from 'app/main/setting/user/user.module';
import { LaporanModule } from './main/laporan/laporan.module';
import { ExampleModule } from './main/example/example.module';

//Errors
import { Error404Module } from 'app/main/errors/404/error-404.module';
import { Error500Module } from 'app/main/errors/500/error-500.module';


//auth
import { LoginModule } from './main/auth/login/login.module';
import { LogoutComponent } from './main/auth/logout/logout.component';
import { RegisterModule } from './main/auth/register/register.module';

import { from } from 'rxjs';
import { LogoutModule } from './main/auth/logout/logout.module';
import { ToastrModule } from 'ngx-toastr';

const appRoutes: Routes = [
    {
        path: '**',
        redirectTo: 'auth/login'
    }
];

@NgModule({
    declarations: [
        AppComponent,
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        RouterModule.forRoot(appRoutes),

        TranslateModule.forRoot(),

        ToastrModule.forRoot({
            timeOut: 10000,
            // toast-top-full-width
            //top-Right
            positionClass: 'toast-bottom-right',
            preventDuplicates: true,
            closeButton: true,
        }),

        // Material moment date module
        MatMomentDateModule,

        // Material
        MatButtonModule,
        MatIconModule,

        // Fuse modules
        FuseModule.forRoot(fuseConfig),
        FuseProgressBarModule,
        FuseSharedModule,
        FuseSidebarModule,
        FuseThemeOptionsModule,

        // Errors
        Error404Module,
        Error500Module,

        //auth
        LoginModule,
        RegisterModule,
        LogoutModule,

        // App modules
        LayoutModule,
        SampleModule,
        MasukModule,
        TarifModule,
        LaporanModule,
        UserModule,
        KeluarModule,
        ExampleModule,

    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule {
}
