import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation, Inject } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject, fromEvent, merge, Observable, Subject, from } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, startWith, tap } from 'rxjs/operators';

import { fuseAnimations } from '@fuse/animations';
import { FuseUtils } from '@fuse/utils';

import { takeUntil } from 'rxjs/internal/operators';
import { MatTableDataSource, MatDialog, ErrorStateMatcher, MatDialogRef, MAT_DIALOG_DATA, MatTable, MatAutocompleteSelectedEvent } from '@angular/material';
import { HttpClient, HttpHeaders, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormControl, FormGroupDirective, NgForm, FormGroup, Validators, FormBuilder, AbstractControl, FormArray } from '@angular/forms';
import { environment } from 'environments/environment';
import { ToastrService } from 'ngx-toastr';

import { DomSanitizer } from '@angular/platform-browser';

import { locale as english } from './i18n/en';
import { locale as turkish } from './i18n/tr';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { FuseProgressBarService } from '@fuse/components/progress-bar/progress-bar.service';
import { Router } from '@angular/router';
import { ApiService } from 'app/service/api.service';
import { FuseNavigationService } from '@fuse/components/navigation/navigation.service';
import * as moment from 'moment';


@Component({
  selector: 'keluar',
  templateUrl: './keluar.component.html',
  styleUrls: ['./keluar.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class KeluarComponent implements OnInit {
  displayedColumns = ['plat', 'jenis', 'waktu'];
  dataSource: MatTableDataSource<any>;
  obs: Observable<any>;
  checkin: any = [];
  @ViewChild(MatPaginator, { static: true })
  paginator: MatPaginator;
  @ViewChild(MatSort, { static: true })
  sort: MatSort;
  /**
   * Constructor
   *
   * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
   */
  constructor(
    private _fuseTranslationLoaderService: FuseTranslationLoaderService,
    private _fuseProgressBarService: FuseProgressBarService,
    http: HttpClient,
    private API: ApiService,
    public _MatDialog: MatDialog) {
    this._fuseTranslationLoaderService.loadTranslations(english, turkish);
  }

  ngOnInit() {
    this.loadCheckin();
  }

  loadCheckin(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.API.chekin_get().subscribe((result) => {
        if (result['status'] === 200) {
          this.checkin = result['result'];
          this.dataSource = new MatTableDataSource(this.checkin);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
          this.obs = this.dataSource.connect();
        }
        resolve(this.checkin);
      }, reject);
    });
  }

  OnAdd() {
    const dialogRef = this._MatDialog.open(addcheckout, {
      panelClass: 'add-checkout',
      hasBackdrop: true,
      width: '50%',
      data: {
        action: 'new',
      }
      // height: '90%',
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.event === "save") {
        this.loadCheckin();
      } else {
        //do something
      }
    });
  }
}

@Component({
  selector: 'add-checkout',
  templateUrl: 'add-checkout/add-checkout.component.html',
  styleUrls: ['./add-checkout/add-checkout.component.scss'],
  animations: fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class addcheckout {
  checkinForm: FormGroup;
  ModelCheckin: any = [];
  constructor(
    private formBuilder: FormBuilder,
    http: HttpClient, private API: ApiService,
    public _MatDialog: MatDialog, private fb: FormBuilder,
    public dialogRef: MatDialogRef<addcheckout>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
  }

  ngOnInit() {
    this.checkinForm = new FormGroup({
      plat_kendaraan: new FormControl(''),
      jenis_kendaraan: new FormControl(''),
    });
  }

  onSave() {
    this.API.chekin_post({
      plat_kendaraan: this.ModelCheckin.plat_kendaraan,
      jenis_kendaraan: this.ModelCheckin.jenis_kendaraan.toLowerCase()
    }).subscribe(result => {
      if (result['status'] === 200) {
        this.dialogRef.close({
          event: 'save'
        })
      }
    });
  }
}
export const COMPONENT_LIST = [
  KeluarComponent, addcheckout
]