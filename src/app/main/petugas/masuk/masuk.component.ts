import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation, Inject } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject, fromEvent, merge, Observable, Subject, from } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, startWith, tap } from 'rxjs/operators';

import { fuseAnimations } from '@fuse/animations';
import { FuseUtils } from '@fuse/utils';

import { takeUntil } from 'rxjs/internal/operators';
import { MatTableDataSource, MatDialog, ErrorStateMatcher, MatDialogRef, MAT_DIALOG_DATA, MatTable, MatAutocompleteSelectedEvent } from '@angular/material';
import { HttpClient, HttpHeaders, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormControl, FormGroupDirective, NgForm, FormGroup, Validators, FormBuilder, AbstractControl, FormArray } from '@angular/forms';
import { environment } from 'environments/environment';
import { ToastrService } from 'ngx-toastr';

import { DomSanitizer } from '@angular/platform-browser';

import { locale as english } from './i18n/en';
import { locale as turkish } from './i18n/tr';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { FuseProgressBarService } from '@fuse/components/progress-bar/progress-bar.service';
import { Router } from '@angular/router';
import { ApiService } from 'app/service/api.service';
import { FuseNavigationService } from '@fuse/components/navigation/navigation.service';
import * as moment from 'moment';

@Component({
  selector: 'masuk',
  templateUrl: './masuk.component.html',
  styleUrls: ['./masuk.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class MasukComponent implements OnInit {
  displayedColumns = ['plat', 'jenis', 'waktu', 'waktu2', 'status', 'aksi'];
  dataSource: MatTableDataSource<iParkir>;
  obs: Observable<any>;
  checkin: iParkir[] = [];
  
  role:any;
  mobil:any;
  motor:any;
  @ViewChild(MatPaginator, { static: true })
  paginator: MatPaginator;
  @ViewChild(MatSort, { static: true })
  sort: MatSort;
  /**
   * Constructor
   *
   * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
   */
  constructor(
    private _fuseTranslationLoaderService: FuseTranslationLoaderService,
    private _fuseProgressBarService: FuseProgressBarService,
    private _fuseNavigationService: FuseNavigationService,
    http: HttpClient,
    private API: ApiService,
    private router: Router,
    public _MatDialog: MatDialog) {
    this._fuseTranslationLoaderService.loadTranslations(english, turkish);
  }

  
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  ngOnInit() {
    this.loadCheckin();
  }

  loadCheckin(): Promise<any> {
    this.checkin = [];
    return new Promise((resolve, reject) => {
      this.API.chekin_get().subscribe((result) => {
        if (result['status'] === 200) {
          result['result'].forEach(e => {
            this.checkin.push(getListparkir(e));
          });
          this.mobil = this.checkin.filter(item=> item.jenis_kendaraan === 'mobil' && item.status === 'in').length;
          this.motor = this.checkin.filter(item=> item.jenis_kendaraan === 'motor' && item.status === 'in').length;
          console.log(this.mobil);
          console.log(this.motor);

          this.dataSource = new MatTableDataSource(this.checkin);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
          this.obs = this.dataSource.connect();
        }
        resolve(this.checkin);
      }, reject);
    });
  }

  OnAdd() {
    const dialogRef = this._MatDialog.open(addcheckin, {
      panelClass: 'add-checkin',
      hasBackdrop: true,
      width: '50%',
      data: {
        action: 'new',
        mobil_length : this.mobil,
        motor_length : this.motor,
      }
      // height: '90%',
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.event === "save") {
        this.loadCheckin();
      } else {
        //do something
      }
    });
  }

  checkout(e){
    const dialogRef = this._MatDialog.open(addcheckout, {
      panelClass: 'add-checkout',
      hasBackdrop: true,
      width: '50%',
      data: {
        action: 'new',
        item:e,
      }
      // height: '90%',
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      if (result.event === "save") {
        this.loadCheckin();
      } else {
        //do something
      }
    });
  }
}

@Component({
  selector: 'add-checkin',
  templateUrl: 'add-checkin/add-checkin.component.html',
  styleUrls: ['./add-checkin/add-checkin.component.scss'],
  animations: fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class addcheckin {
  checkinForm: FormGroup;
  ModelCheckin: any = [];
  constructor(
    private formBuilder: FormBuilder,
    http: HttpClient, private API: ApiService,
    public _MatDialog: MatDialog, private fb: FormBuilder,
    public dialogRef: MatDialogRef<addcheckin>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
  }

  ngOnInit() {
    this.checkinForm = new FormGroup({
      plat_kendaraan: new FormControl(''),
      jenis_kendaraan: new FormControl(''),
    });
  }

  onSave() {
    if(this.ModelCheckin.jenis_kendaraan.toLowerCase() === 'motor'){
      if(this.data.motor_length === 60){
        alert('Parkir Full');
      }else{
        this.API.chekin_post({
          plat_kendaraan: this.ModelCheckin.plat_kendaraan,
          jenis_kendaraan: this.ModelCheckin.jenis_kendaraan.toLowerCase()
        }).subscribe(result => {
          if (result['status'] === 200) {
            this.dialogRef.close({
              event: 'save'
            })
          }
        });
      }
    }else{
      if(this.data.mobil_length === 30){
        alert('Parkir Full');
      }else{
        this.API.chekin_post({
          plat_kendaraan: this.ModelCheckin.plat_kendaraan,
          jenis_kendaraan: this.ModelCheckin.jenis_kendaraan.toLowerCase()
        }).subscribe(result => {
          if (result['status'] === 200) {
            this.dialogRef.close({
              event: 'save'
            })
          }
        });
      }
    }

  }
}


@Component({
  selector: 'add-checkout',
  templateUrl: 'add-checkout/add-checkout.component.html',
  styleUrls: ['./add-checkout/add-checkout.component.scss'],
  animations: fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class addcheckout {
  checkinForm: FormGroup;
  ModelCheckin: any = [];
  tarif:any = [];
  constructor(
    private formBuilder: FormBuilder,
    http: HttpClient, private API: ApiService,
    public _MatDialog: MatDialog, private fb: FormBuilder,
    public dialogRef: MatDialogRef<addcheckout>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
  }

  ngOnInit() {
    this.ModelCheckin = this.data.item;
    // console.log(this.ModelCheckin.plat_kendaraan);
    this.API.checkout_post({
      plat_kendaraan : this.ModelCheckin.plat_kendaraan
    }).subscribe(result=>{
      this.tarif = result;
    });

    this.checkinForm = new FormGroup({
      plat_kendaraan: new FormControl(''),
      jenis_kendaraan: new FormControl(''),
    });
  }

  onSave() {
    this.dialogRef.close({
      event:'close'
    })
    // this.API.chekin_post({
    //   plat_kendaraan: this.ModelCheckin.plat_kendaraan,
    //   jenis_kendaraan: this.ModelCheckin.jenis_kendaraan.toLowerCase()
    // }).subscribe(result => {
    //   if (result['status'] === 200) {
    //     this.dialogRef.close({
    //       event: 'save'
    //     })
    //   }
    // });
  }

  onPrint(){
    this.dialogRef.close({event:'save'});
    const dialogRef = this._MatDialog.open(printdialog, {
      panelClass: 'add-checkout',
      hasBackdrop: true,
      width: '50%',
      data: {
        item:this.ModelCheckin,
        tarif : this.tarif,
      }
      // height: '90%',
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.event === "save") {
        // this.loadCheckin();
      } else {
        //do something
      }
    });
  }
}

@Component({
  selector: 'printdialog',
  templateUrl: 'printdialog.html',
  styleUrls: ['./masuk.component.scss'],
  animations: fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class printdialog {
  checkinForm: FormGroup;
  ModelCheckin: any = [];
  today: number = Date.now();
  constructor(
    private formBuilder: FormBuilder,
    http: HttpClient, private API: ApiService,
    public _MatDialog: MatDialog, private fb: FormBuilder,
    public dialogRef: MatDialogRef<printdialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
  }

  ngOnInit() {
    console.log(this.data);
  }
}

function getListparkir(data:any):iParkir{
  // console.log(data);
  return{
    id_cekin : data['id_cekin'],
    plat_kendaraan: data['plat_kendaraan'],
    jenis_kendaraan: data['jenis_kendaraan'],
    waktu: data['waktu'],
    waktu2: data['waktu2'],
    status: data['status'],
  }
}
export interface iParkir{
  id_cekin:string,
  plat_kendaraan:string,
  jenis_kendaraan:string,
  waktu:Date,
  waktu2:string,
  status:string,

}
export const COMPONENT_LIST = [
  MasukComponent, addcheckin, addcheckout, printdialog
]