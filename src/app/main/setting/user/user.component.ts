import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation, Inject } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject, fromEvent, merge, Observable, Subject, from } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, startWith, tap } from 'rxjs/operators';

import { fuseAnimations } from '@fuse/animations';
import { FuseUtils } from '@fuse/utils';

import { takeUntil } from 'rxjs/internal/operators';
import { MatTableDataSource, MatDialog, ErrorStateMatcher, MatDialogRef, MAT_DIALOG_DATA, MatTable, MatAutocompleteSelectedEvent } from '@angular/material';
import { HttpClient, HttpHeaders, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormControl, FormGroupDirective, NgForm, FormGroup, Validators, FormBuilder, AbstractControl, FormArray } from '@angular/forms';
import { environment } from 'environments/environment';
import { ToastrService } from 'ngx-toastr';

import { DomSanitizer } from '@angular/platform-browser';

import { locale as english } from './i18n/en';
import { locale as turkish } from './i18n/tr';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { FuseProgressBarService } from '@fuse/components/progress-bar/progress-bar.service';
import { Router } from '@angular/router';
import { ApiService } from 'app/service/api.service';
import { FuseNavigationService } from '@fuse/components/navigation/navigation.service';
import * as moment from 'moment';

@Component({
  selector: 'user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class UserComponent implements OnInit {
  displayedColumns = ['nama', 'username', 'role'];
  dataSource: MatTableDataSource<any>;
  obs: Observable<any>;
  listuser: any = [];

  role:any;

  @ViewChild(MatPaginator, { static: true })
  paginator: MatPaginator;
  @ViewChild(MatSort, { static: true })
  sort: MatSort;
  /**
   * Constructor
   *
   * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
   */
  constructor(
    private _fuseTranslationLoaderService: FuseTranslationLoaderService,
    private _fuseProgressBarService: FuseProgressBarService,
    private _fuseNavigationService: FuseNavigationService,
    http: HttpClient,
    private router: Router,
    private API: ApiService,
    public _MatDialog: MatDialog) {
    this._fuseTranslationLoaderService.loadTranslations(english, turkish);
  }


  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  ngOnInit() {
    // this.role = JSON.parse(localStorage.getItem('ROLE'));
    // if(this.role !== 'petugas'){
    //   this._fuseNavigationService.updateNavigationItem('masuk',  {
    //     hidden: true
    //   });
    // }else{
    //   this.router.navigate(['/auth/login']);
    // }

    this.loaduser();
  }

  loaduser(): Promise<any> {
    this.listuser = [];
    return new Promise((resolve, reject) => {
      this.API.user_get().subscribe((result) => {
        if (result['status'] === 200) {
          this.listuser = result['result'];
          this.dataSource = new MatTableDataSource(this.listuser);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
          console.log(this.dataSource);
          this.obs = this.dataSource.connect();
        }
        resolve(this.listuser);
      }, reject);
    });
  }

  OnAdd() {
    const dialogRef = this._MatDialog.open(userdialog, {
      panelClass: 'user-dialog',
      hasBackdrop: true,
      width: '50%',
      data: {
        action: 'new',
      }
      // height: '90%',
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      if (result.event === "save") {
        this.loaduser();
      } else {
        //do something
      }
    });
  }

  onEdit(e) {
    const dialogRef = this._MatDialog.open(userdialog, {
      panelClass: 'user-dialog',
      hasBackdrop: true,
      width: '50%',
      data: {
        action: 'edit',
        item: e,
      }
      // height: '90%',
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      if (result.event === "save") {
        this.loaduser();
      } else {
        //do something
      }
    });
  }
}
@Component({
  selector: 'user-dialog',
  templateUrl: 'user-dialog/user-dialog.html',
  styleUrls: ['./user-dialog/user-dialog.scss'],
  animations: fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class userdialog {
  userform: FormGroup;
  ModelUser: any = [];
  constructor(
    private formBuilder: FormBuilder,
    http: HttpClient, private API: ApiService,
    public _MatDialog: MatDialog, private fb: FormBuilder,
    public dialogRef: MatDialogRef<userdialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
  }

  ngOnInit() {
    this.userform = new FormGroup({
      nama: new FormControl(''),
      username: new FormControl(''),
      password: new FormControl(''),
      role: new FormControl(''),
    });

    console.log(this.data);
    if (this.data.action === 'new') {
      this.ModelUser = [];
    } else {
      this.ModelUser = this.data.item;
    }
  }

  onSave() {
    console.log(this.ModelUser);
    if (this.data.action === 'new') {
      this.API.user_post({
        nama: this.ModelUser.nama,
        username: this.ModelUser.username,
        password: this.ModelUser.passwords,
        role: this.ModelUser.role,
      }).subscribe(result => {
        console.log(result);
        if (result['status'] === 200) {
          this.dialogRef.close({
            event: "save"
          })
        }
      })
    } else {
      this.API.user_put(this.ModelUser.id_user, {
        nama: this.ModelUser.nama,
        username: this.ModelUser.username,
        password: this.ModelUser.passwords,
        role: this.ModelUser.role,
      }).subscribe(result => {
        console.log(result);
        if (result['status'] === 200) {
          this.dialogRef.close({
            event: "save"
          })
        }
      })
    }
  }
}
export const COMPONENT_LIST = [
  UserComponent, userdialog
]