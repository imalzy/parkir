import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation, Inject } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject, fromEvent, merge, Observable, Subject, from } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, startWith, tap } from 'rxjs/operators';

import { fuseAnimations } from '@fuse/animations';
import { FuseUtils } from '@fuse/utils';

import { takeUntil } from 'rxjs/internal/operators';
import { MatTableDataSource, MatDialog, ErrorStateMatcher, MatDialogRef, MAT_DIALOG_DATA, MatTable, MatAutocompleteSelectedEvent } from '@angular/material';
import { HttpClient, HttpHeaders, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormControl, FormGroupDirective, NgForm, FormGroup, Validators, FormBuilder, AbstractControl, FormArray } from '@angular/forms';
import { environment } from 'environments/environment';
import { ToastrService } from 'ngx-toastr';

import { DomSanitizer } from '@angular/platform-browser';

import { locale as english } from './i18n/en';
import { locale as turkish } from './i18n/tr';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { FuseProgressBarService } from '@fuse/components/progress-bar/progress-bar.service';
import { Router } from '@angular/router';
import { ApiService } from 'app/service/api.service';
import { FuseNavigationService } from '@fuse/components/navigation/navigation.service';
import * as moment from 'moment';
import { NgIf } from '@angular/common';

@Component({
  selector: 'tarif',
  templateUrl: './tarif.component.html',
  styleUrls: ['./tarif.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class TarifComponent implements OnInit {
  displayedColumns = ['jenis_kendaraan', 'tarif_1_jam', 'tarif_lebih_dari_1_jam', 'bayar_perhari', 'harian'];
  dataSource: MatTableDataSource<any>;
  obs: Observable<any>;
  tarif: any = [];
  role:any;

  @ViewChild(MatPaginator, { static: true })
  paginator: MatPaginator;
  @ViewChild(MatSort, { static: true })
  sort: MatSort;
  /**
   * Constructor
   *
   * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
   */
  constructor(
    private _fuseTranslationLoaderService: FuseTranslationLoaderService,
    private _fuseProgressBarService: FuseProgressBarService,
    private _fuseNavigationService: FuseNavigationService,
    http: HttpClient,
    private API: ApiService,
    private router: Router,
    public _MatDialog: MatDialog) {
    this._fuseTranslationLoaderService.loadTranslations(english, turkish);
  }


  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  ngOnInit() {
    // this.role = JSON.parse(localStorage.getItem('ROLE'));
    // if(this.role !== 'petugas'){
    //   this._fuseNavigationService.updateNavigationItem('masuk',  {
    //     hidden: true
    //   });
    // }else{
    //   this.router.navigate(['/auth/login']);
    // }
    this.loadTarif();
  }

  loadTarif(): Promise<any> {
    this.tarif = [];
    return new Promise((resolve, reject) => {
      this.API.tarif_get().subscribe((result) => {
        if (result['status'] === 200) {
          this.tarif = result['result'];
          this.dataSource = new MatTableDataSource(this.tarif);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
          // console.log(this.dataSource);
          this.obs = this.dataSource.connect();
        }
        resolve(this.tarif);
      }, reject);
    });
  }

  OnAdd(){
    const dialogRef = this._MatDialog.open(addTarif, {
      panelClass: 'addTarif',
      hasBackdrop: true,
      width: '50%',
      data: {
        action: 'new',
      }
      // height: '90%',
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      if (result.event === "save") {
        this.loadTarif();
      } else {
        //do something
      }
    });
  }

  onEdit(e) {
    const dialogRef = this._MatDialog.open(addTarif, {
      panelClass: 'addTarif',
      hasBackdrop: true,
      width: '50%',
      data: {
        action: 'edit',
        item: e,
      }
      // height: '90%',
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      if (result.event === "save") {
        this.loadTarif();
      } else {
        //do something
      }
    });
  }
}

@Component({
  selector: 'addTarif',
  templateUrl: 'addTarif/addTarif.html',
  styleUrls: ['./addTarif/addTarif.scss'],
  animations: fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class addTarif {
  tarif_form: FormGroup;
  ModelTarif: any = [];
  constructor(
    private formBuilder: FormBuilder,
    http: HttpClient, private API: ApiService,
    public _MatDialog: MatDialog, private fb: FormBuilder,
    public dialogRef: MatDialogRef<addTarif>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
  }

  ngOnInit() {
    // console.log(this.data);
    this.tarif_form = new FormGroup({
      jenis_kendaraan: new FormControl(''),
      tarif_1_jam: new FormControl(''),
      tarif_lebih_dari_1_jam: new FormControl(''),
      bayar_perhari: new FormControl(''),
      harian: new FormControl(''),
    });

    if (this.data.action === 'new') {
      this.ModelTarif = [];
    } else {
      this.ModelTarif = this.data.item;
    }
  }

  onSave() {
    if (this.data.action === 'new') {
      this.API.tarif_post({
        jenis_kendaraan: this.ModelTarif.jenis_kendaraan,
        tarif_1_jam: this.ModelTarif.tarif_1_jam,
        tarif_lebih_dari_1_jam: this.ModelTarif.tarif_lebih_dari_1_jam,
        bayar_perhari: this.ModelTarif.bayar_perhari,
        harian: this.ModelTarif.harian
      }).subscribe(result=>{
        if(result['status']===200){
          this.dialogRef.close({event:"save"});
        }
      })
    } else {
      this.API.tarif_put(this.data.item.id_tarif, {
        jenis_kendaraan: this.ModelTarif.jenis_kendaraan,
        tarif_1_jam: this.ModelTarif.tarif_1_jam,
        tarif_lebih_dari_1_jam: this.ModelTarif.tarif_lebih_dari_1_jam,
        bayar_perhari: this.ModelTarif.bayar_perhari,
        harian: this.ModelTarif.harian
      }).subscribe(result => {
        if(result['status']===200){
          this.dialogRef.close({event:"save"});
        }
      })
    }
  }
}
export const COMPONENT_LIST = [
  TarifComponent, addTarif
]