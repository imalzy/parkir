import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExampleComponent } from './example.component';
import { RouterModule, Routes } from '@angular/router';

const routes = [
  {
    path: 'example',
    component: ExampleComponent
  }
];

@NgModule({
  declarations: [
    ExampleComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ],
  exports:[
    ExampleComponent,
  ]
})
export class ExampleModule { }
