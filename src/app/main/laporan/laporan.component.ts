import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation, Inject } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject, fromEvent, merge, Observable, Subject, from } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, startWith, tap } from 'rxjs/operators';

import { fuseAnimations } from '@fuse/animations';
import { FuseUtils } from '@fuse/utils';

import { takeUntil } from 'rxjs/internal/operators';
import { MatTableDataSource, MatDialog, ErrorStateMatcher, MatDialogRef, MAT_DIALOG_DATA, MatTable, MatAutocompleteSelectedEvent } from '@angular/material';
import { HttpClient, HttpHeaders, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormControl, FormGroupDirective, NgForm, FormGroup, Validators, FormBuilder, AbstractControl, FormArray } from '@angular/forms';
import { environment } from 'environments/environment';
import { ToastrService } from 'ngx-toastr';

import { DomSanitizer } from '@angular/platform-browser';

import { locale as english } from './i18n/en';
import { locale as turkish } from './i18n/tr';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { FuseProgressBarService } from '@fuse/components/progress-bar/progress-bar.service';
import { Router } from '@angular/router';
import { ApiService } from 'app/service/api.service';
import { FuseNavigationService } from '@fuse/components/navigation/navigation.service';
import * as moment from 'moment';

@Component({
  selector: 'laporan',
  templateUrl: './laporan.component.html',
  styleUrls: ['./laporan.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class LaporanComponent implements OnInit {
  displayedColumns = ['plat', 'jenis', 'waktu', 'waktu2', 'status'];
  dataSource: MatTableDataSource<iParkir>;
  obs: Observable<any>;
  checkin: iParkir[] = [];

  DateNow:any;
  localstorage: any = [];
  reportPrint: any = [];
  pil_bulan: any;
  jenis_kendaraan: any;
  listMonth = [
    {
      'id': 1,
      'month': "January",
    },
    {
      'id': 2,
      'month': "February",
    },
    {
      'id': 3,
      'month': "March",
    },
    {
      'id': 4,
      'month': "April",
    },
    {
      'id': 5,
      'month': "May",
    },
    {
      'id': 6,
      'month': "June",
    },
    {
      'id': 7,
      'month': "July",
    },
    {
      'id': 8,
      'month': "August",
    },
    {
      'id': 9,
      'month': "October",
    },
    {
      'id': 10,
      'month': "November",
    },
    {
      'id': 11,
      'month': "December",
    },
  ];


  @ViewChild(MatPaginator, { static: true })
  paginator: MatPaginator;
  @ViewChild(MatSort, { static: true })
  sort: MatSort;
  /**
   * Constructor
   *
   * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
   */
  constructor(
    private _fuseTranslationLoaderService: FuseTranslationLoaderService,
    private _fuseNavigationService: FuseNavigationService,
    private _fuseProgressBarService: FuseProgressBarService,
    http: HttpClient,
    private API: ApiService,
    private router: Router,
    public _MatDialog: MatDialog) {
    this._fuseTranslationLoaderService.loadTranslations(english, turkish);
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  ngOnInit() {
    this.DateNow = new Date();
    this.loadCheckin();
  }

  loadCheckin(): Promise<any> {
    this.checkin = [];
    return new Promise((resolve, reject) => {
      this.API.chekin_get().subscribe((result) => {
        if (result['status'] === 200) {
          result['result'].forEach(e => {
            if (e.status === 'out') {
              this.checkin.push(getListparkir(e));
            }
          });
          this.dataSource = new MatTableDataSource(this.checkin);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
          // console.log(new Date(this.checkin[0].waktu2).getDate());
          this.obs = this.dataSource.connect();
        }
        resolve(this.checkin);
      }, reject);
    });
  }

  onChange(e) {
    this.reportPrint = [];
    if (e !== 'null') {
      this.reportPrint = this.checkin.filter(item => new Date(item.waktu2).getMonth() === Number(e));
      this.dataSource = new MatTableDataSource(this.reportPrint);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    } else {
      this.loadCheckin();
    }
  }

  onChange2(e) {
    this.reportPrint = [];
    if (e !== 'all') {
      this.reportPrint = this.checkin.filter(item => item.jenis_kendaraan === e.toString());
      this.dataSource = new MatTableDataSource(this.reportPrint);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    } else {
      this.loadCheckin();
    }
  }

  onDateSelected(newdate){
    this.reportPrint = [];
    const _ = moment();
    const tmp = moment(newdate).add({ hours: _.hour(), minutes: _.minute(), seconds: _.second() });
    let tmpselected =  moment(tmp.toDate()).format('D').toString();
    if(tmpselected !==''){
      // this.reportPrint = this.checkin.forEach(item=> console.log(new Date(item.waktu2).getDate().toString()));
      this.reportPrint = this.checkin.filter(item=> new Date(item.waktu2).getDate().toString() === tmpselected);
      this.dataSource = new MatTableDataSource(this.reportPrint);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
    // console.log(tmpselected);
  }

  Print() {
    const dialogRef = this._MatDialog.open(printdialog, {
      panelClass: 'printdialog',
      hasBackdrop: true,
      width: '50%',
      data: {
        item: this.reportPrint,
      }
      // height: '90%',
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.event === "save") {
        this.loadCheckin();
      } else {
        //do something
      }
    });
  }
}
@Component({
  selector: 'printdialog',
  templateUrl: 'printdialog.html',
  styleUrls: ['./printdialog.scss'],
  animations: fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class printdialog {
  checkinForm: FormGroup;
  ModelCheckin: any = [];
  today: number = Date.now();
  constructor(
    private formBuilder: FormBuilder,
    http: HttpClient, private API: ApiService,
    public _MatDialog: MatDialog, private fb: FormBuilder,
    public dialogRef: MatDialogRef<printdialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
  }

  ngOnInit() {
    console.log(this.data);
  }
}
function getListparkir(data: any): iParkir {
  return {
    id_cekin: data['id_cekin'],
    plat_kendaraan: data['plat_kendaraan'],
    jenis_kendaraan: data['jenis_kendaraan'],
    waktu: data['waktu'],
    waktu2: data['waktu2'],
    status: data['status'],
  }
}
export interface iParkir {
  id_cekin: string,
  plat_kendaraan: string,
  jenis_kendaraan: string,
  waktu: Date,
  waktu2: string,
  status: string,

}
export const COMPONENT_LIST = [
  LaporanComponent, printdialog
]