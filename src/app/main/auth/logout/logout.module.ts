import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { FuseProgressBarModule, FuseSearchBarModule, FuseWidgetModule, FuseNavigationModule } from '@fuse/components';
import { FuseSharedModule } from '@fuse/shared.module';

import { LogoutComponent } from 'app/main/auth/logout/logout.component';
import { AppOverlayModule } from '../../../overlay/overlay.module';
import { ProgressSpinnerModule, ProgressSpinnerComponent } from '../../../progress-spinner/progress-spinner.module';
import {  MatAutocompleteModule, MatBadgeModule, MatBottomSheetModule, 
          MatButtonToggleModule, MatCardModule, MatChipsModule, MatDatepickerModule, 
          MatDialogModule, MatDividerModule, MatExpansionModule, MatGridListModule, 
          MatListModule, MatMenuModule, MatPaginatorModule, MatProgressBarModule, 
          MatProgressSpinnerModule, MatRadioModule, MatRippleModule, MatSelectModule, 
          MatSidenavModule, MatSliderModule, MatSlideToggleModule, MatSnackBarModule, 
          MatSortModule, MatStepperModule, MatTableModule, MatTabsModule, MatToolbarModule, 
          MatTooltipModule, MatTreeModule } from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';

const routes = [
  {
      path     : 'auth/logout',
      component: LogoutComponent
  }
];

@NgModule({
  declarations: [
    LogoutComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    FuseSharedModule,
    AppOverlayModule,
    ProgressSpinnerModule,
    MatAutocompleteModule, MatBadgeModule, MatBottomSheetModule, MatButtonModule,
    MatButtonToggleModule, MatCardModule, MatCheckboxModule, MatChipsModule, MatDatepickerModule,
    MatDialogModule, MatDividerModule, MatExpansionModule, MatFormFieldModule, MatGridListModule,
    MatIconModule, MatInputModule, MatListModule, MatMenuModule, MatPaginatorModule,
    MatProgressBarModule, MatProgressSpinnerModule, MatRadioModule, MatRippleModule, MatSelectModule,
    MatSidenavModule, MatSliderModule, MatSlideToggleModule, MatSnackBarModule, MatSortModule,
    MatStepperModule, MatTableModule, MatTabsModule, MatToolbarModule, MatTooltipModule, MatTreeModule, BrowserModule,

],
exports: [
    LogoutComponent,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSlideToggleModule,
    MatSliderModule,
    MatSnackBarModule,
    MatSortModule,
    MatStepperModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
    //fuse
    FuseSharedModule,
    FuseSearchBarModule,
    FuseProgressBarModule,
    FuseWidgetModule,
    FuseNavigationModule,
    
]
})
export class LogoutModule { }
