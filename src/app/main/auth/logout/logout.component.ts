import { Component, ViewChild, Inject, ViewEncapsulation, OnInit, Renderer2, ElementRef, ChangeDetectionStrategy, Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { fuseAnimations } from '@fuse/animations';
import { FuseConfigService } from '@fuse/services/config.service';
import { FuseNavigationService } from '@fuse/components/navigation/navigation.service';
import { MatPaginator, MatSort, MatTableDataSource, MatProgressSpinnerModule, MatDialog, MatDialogRef, MAT_DIALOG_DATA, throwMatDuplicatedDrawerError } from '@angular/material';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { ApiService } from 'app/service/api.service';
import { first } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';


@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss']
})
export class LogoutComponent implements OnInit {

  /**
 * Constructor
 *
 * @param {FuseConfigService} _fuseConfigService
 * @param {FormBuilder} _formBuilder
 */
  constructor(
    private _fuseConfigService: FuseConfigService,
    private _fuseNavigationService: FuseNavigationService,
    private _formBuilder: FormBuilder,
    private router: Router,
    private API: ApiService,
  ){}

  ngOnInit() {
    localStorage.clear();
    this.router.navigate(['/auth/login']);
  }

}
