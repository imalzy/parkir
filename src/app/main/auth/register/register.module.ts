import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';

import { FuseSharedModule } from '@fuse/shared.module';

import { RegisterComponent } from 'app/main/auth/register/register.component';
import { AppOverlayModule } from '../../../overlay/overlay.module';
import { ProgressSpinnerModule, ProgressSpinnerComponent } from '../../../progress-spinner/progress-spinner.module';
const routes = [
    {
        path     : 'auth/register',
        component: RegisterComponent
    }
];

@NgModule({
    declarations: [
      RegisterComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        AppOverlayModule,
        ProgressSpinnerModule,
        
        MatButtonModule,
        MatCheckboxModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,

        FuseSharedModule
    ]
})
export class RegisterModule { }
