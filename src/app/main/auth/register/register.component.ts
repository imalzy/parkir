import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators, FormControl } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { ApiService } from 'app/service/api.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class RegisterComponent implements OnInit {

    registerForm: FormGroup;
    weatherData : Object

    color = 'accent';
    mode = 'indeterminate';
    value = 80;
    spinnerWithoutBackdrop = false;
    // Private
    private _unsubscribeAll: Subject<any>;

    constructor(
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder,
        http: HttpClient,
        private router: Router,
        private API: ApiService,
        private toastr: ToastrService,
    )
    {
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar   : {
                    hidden: true
                },
                toolbar  : {
                    hidden: true
                },
                footer   : {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        // this.registerForm = this._formBuilder.group({
        //     name           : ['', Validators.required],
        //     email          : ['', [Validators.required, Validators.email]],
        //     password       : ['', Validators.required],
        //     passwordConfirm: ['', [Validators.required, confirmPasswordValidator]]
        // });
        this.registerForm = new FormGroup({
            name           : new FormControl('', [Validators.required]),
            email          : new FormControl('',  [Validators.required, Validators.email]),
            password       : new FormControl('', [Validators.required]),
            passwordConfirm: new FormControl('', [Validators.required, confirmPasswordValidator]),
        });
        // Update the validity of the 'passwordConfirm' field
        // when the 'password' field changes
        this.registerForm.get('password').valueChanges
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
                this.registerForm.get('passwordConfirm').updateValueAndValidity();
            });
    }

    // onCreate(){
    //     this.loadSpinner();
    //     this.API.account_post(this.registerForm.value.name, this.registerForm.value.email, this.registerForm.value.password).subscribe(result=>{
    //         // console.log(result);
    //         if (result['status'] === true) {
    //             this.toastr.success("Registrasi Berhasil", "Informasi");
    //             this.closeSpinner();
    //             setTimeout(() => {
    //                 this.router.navigate(['/auth/login']);
    //             }, 1000);
    //         } 
    //     }, (error:HttpErrorResponse) => {
    //         this.closeSpinner();
    //         this.toastr.warning(error.statusText, "Informasi");
    //         // if(error.status===400){    
    //         //     this.toastr.warning(error.message, "Informasi");
    //         //     const errors: Array<ClientError> = error.error;
    //         //     console.log(errors);
    //         //     errors.forEach(clientError => {
    //         //         console.log(clientError.code);
    //         //     });
    //         // }
    //     }
    //     );
    // }

    loadSpinner() {
        this.spinnerWithoutBackdrop = true;
    }
    closeSpinner() {
        this.spinnerWithoutBackdrop = false;
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
}
/**
 * Confirm password validator
 *
 * @param {AbstractControl} control
 * @returns {ValidationErrors | null}
 */
export const confirmPasswordValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

    if ( !control.parent || !control )
    {
        return null;
    }

    const password = control.parent.get('password');
    const passwordConfirm = control.parent.get('passwordConfirm');

    if ( !password || !passwordConfirm )
    {
        return null;
    }

    if ( passwordConfirm.value === '' )
    {
        return null;
    }

    if ( password.value === passwordConfirm.value )
    {
        return null;
    }

    return {passwordsNotMatching: true};
};

interface ClientError {
    code: string;
    description: string;
}