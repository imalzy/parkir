import { Component, ViewChild, Inject, ViewEncapsulation, OnInit, Renderer2, ElementRef, ChangeDetectionStrategy, Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { fuseAnimations } from '@fuse/animations';
import { FuseConfigService } from '@fuse/services/config.service';
import { FuseNavigationService } from '@fuse/components/navigation/navigation.service';
import { MatPaginator, MatSort, MatTableDataSource, MatProgressSpinnerModule, MatDialog, MatDialogRef, MAT_DIALOG_DATA, throwMatDuplicatedDrawerError } from '@angular/material';
import { HttpClient, HttpHeaders, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ApiService } from 'app/service/api.service';
import { first } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
@Injectable({
  providedIn: 'root'
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  color = 'accent';
  mode = 'indeterminate';
  value = 80;
  spinnerWithoutBackdrop = false;
  islogged: boolean;
  pesanerror: any = [];

  hidden = false;
  /**
   * Constructor
   *
   * @param {FuseConfigService} _fuseConfigService
   * @param {FormBuilder} _formBuilder
   */
  constructor(
    private _fuseConfigService: FuseConfigService,
    private _fuseNavigationService: FuseNavigationService,
    private _formBuilder: FormBuilder,
    private router: Router,
    private API: ApiService,
    private toastr: ToastrService,
  ) {
    // Configure the layout
    this._fuseConfigService.config = {
      layout: {
        navbar: {
          hidden: true
        },
        toolbar: {
          hidden: true
        },
        footer: {
          hidden: true
        },
        sidepanel: {
          hidden: true
        }
      }
    };
  }

  ngOnInit(): void {
    // var auth = await localStorage.getItem('AUTH');
    // this.loginForm = new FormGroup({
    //   email: new FormControl('', [Validators.required, Validators.email]),
    //   password: new FormControl('', [Validators.required]),
    // });

    this.loginForm = new FormGroup({
      username: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required]),
    });
  }

  loadSpinner() {
    this.spinnerWithoutBackdrop = true;
  }
  closeSpinner() {
    this.spinnerWithoutBackdrop = false;
  }

  isLoggedIn() {
    //    this.islogged = this.afAuth.auth.currentUser;
  }


  login() {
    this.loadSpinner();
    this.API.login_post({
      username: this.loginForm.value.username,
      password: this.loginForm.value.password
    }).subscribe(result => {
      // console.log(result);
      if (result['status'] === 200) {
        localStorage.setItem('ROLE', JSON.stringify(result['result'].role));
        localStorage.setItem('NAME', JSON.stringify(result['result'].nama));

        if (result['result'].role === 'admin') {
          setTimeout(() => {
            this.router.navigate(['/setting/tarif']);
          }, 1000);
        }else{
          setTimeout(() => {
            this.router.navigate(['/checkin']);
            this.hidden = false;
          }, 1000);
        }

        this.toastr.success("Login Success", "Informasi");
        this.closeSpinner();
        // setTimeout(() => {
        //   this.router.navigate(['/petugas-masuk']);
        // }, 1000);
      }else{
        this.toastr.error(result['message'], "Informasi");
        this.closeSpinner();
      }
    }, (error: HttpErrorResponse) => {
      this.pesanerror = error.error.message;
      this.toastr.error(error.error.message, "Informasi");
      this.closeSpinner();
    });
  }
}
interface ClientError {
  code: string;
  description: string;
}